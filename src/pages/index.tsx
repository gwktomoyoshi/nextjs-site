import Image from 'next/image'
import 'node_modules/bootstrap/dist/css/bootstrap.min.css'
import styles from './page.module.css'
import { Icon } from '@iconify/react'
import RootLayout from './layout'
import vercelLogo from '@/assets/vercel.svg'
import nextjsLogo from '@/assets/next.svg'
import vscodeLogo from '@/assets/vscode.svg'
import reactLogo from '@/assets/react.svg'
import typescriptLogo from '@/assets/typescript.svg'
import bootstrapLogo from '@/assets/bootstrap.svg'
import iconifyLogo from '@/assets/iconify.png'
import Button from 'react-bootstrap/Button'

export async function getStaticProps() {
  return {
    props: {},
  };
}

export default function Home() {
  return (
    <RootLayout>
      <main className={styles.main}>
        <div className={styles.description}>
          <p>
            Get started by editing&nbsp;
            <code className={styles.code}>src/pages/index.tsx</code>
          </p>
          <div>
            <a
              href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
              target="_blank"
              rel="noopener noreferrer"
            >
              By{' '}
              <Image
                src={vercelLogo}
                alt="Vercel Logo"
                className={styles.vercelLogo}
                width={100}
                height={24}
                priority
              />
            </a>
          </div>
        </div>

        <div className="flex">
          <a href="https://code.visualstudio.com/" target="_blank">
            <Image
                className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
                src={vscodeLogo}
                alt="VSCode Logo"
                height={48}
              />
          </a>
          <a href="https://ja.legacy.reactjs.org/" target="_blank">
            <Image
                className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
                src={reactLogo}
                alt="React Logo"
                height={48}
              />
          </a>
          <a href="https://www.typescriptlang.org/" target="_blank">
            <Image
                className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
                src={typescriptLogo}
                alt="TypeScript Logo"
                height={48}
              />
          </a>  
          <a href="https://getbootstrap.com/" target="_blank">
            <Image
                className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
                src={bootstrapLogo}
                alt="Bootstrap Logo"
                height={48}
              />
          </a>
          <a href="https://iconify.design/" target="_blank">
            <Image
                className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
                src={iconifyLogo}
                alt="iconify Logo"
                height={48}
              />
          </a>
        </div>

        <div className="flex">
          <Button variant="primary">Button</Button>
        </div>

        <div className={styles.center}>
          <Image
            className={styles.logo}
            src={nextjsLogo}
            alt="Next.js Logo"
            width={180}
            height={37}
            priority
          />
        </div>

        <div className={styles.grid}>
          <a
            href="https://nextjs.org/docs?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            className={styles.card}
            target="_blank"
            rel="noopener noreferrer"
          >
            <h2>
              Docs <span>-&gt;</span>
            </h2>
            <p><Icon icon="fa-solid:book" />Find in-depth information about Next.js features and API.</p>
          </a>

          <a
            href="https://nextjs.org/learn?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            className={styles.card}
            target="_blank"
            rel="noopener noreferrer"
          >
            <h2>
              Learn <span>-&gt;</span>
            </h2>
            <p>Learn about Next.js in an interactive course with&nbsp;quizzes!</p>
          </a>

          <a
            href="https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            className={styles.card}
            target="_blank"
            rel="noopener noreferrer"
          >
            <h2>
              Templates <span>-&gt;</span>
            </h2>
            <p>Explore starter templates for Next.js.</p>
          </a>

          <a
            href="https://vercel.com/new?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            className={styles.card}
            target="_blank"
            rel="noopener noreferrer"
          >
            <h2>
              Deploy <span>-&gt;</span>
            </h2>
            <p>
              Instantly deploy your Next.js site to a shareable URL with Vercel.
            </p>
          </a>
        </div>
      </main>
    </RootLayout>
  )
}
